/*
 * Developed by WSO2 Inc, for SCB Risk Technology, on request.
 */

package com.scbrisktechnology.auditlogs;

import org.apache.commons.logging.Log;
import org.wso2.carbon.CarbonConstants;
import org.wso2.carbon.identity.core.bean.context.MessageContext;
import org.wso2.carbon.identity.core.handler.AbstractIdentityMessageHandler;
import org.wso2.carbon.identity.core.model.IdentityEventListenerConfig;
import org.wso2.carbon.identity.core.util.IdentityUtil;
import org.wso2.carbon.identity.data.publisher.application.authentication.AbstractAuthenticationDataPublisher;
import org.wso2.carbon.identity.data.publisher.application.authentication.model.AuthenticationData;
import org.wso2.carbon.identity.data.publisher.application.authentication.model.SessionData;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Properties;

import static org.osgi.service.useradmin.UserAdminPermission.ADMIN;

/**
 * Authentication Data Publisher which takes the login/logout events and logs into "Audit" log.
 */
public class LoginLogoutAuditDataPublisher extends AbstractAuthenticationDataPublisher {

    private static final Log AUDIT_LOG = CarbonConstants.AUDIT_LOG;
    public static final String ACCESS_TYPE = "Access.Type";
    public static final String GUI = "GUI";
    private static String auditMessage = "#Access_ID=\"%s\"##" +
            "Access_Name=\"%s\"##Access_Type=\"%s\"##Access_Country=\"%s\"##Access_Profile=\"%s\"##" +
            "Access_SourceHost=\"%s\"##Access_SourceIP=\"%s\"##Access_%sTimeStamp=\"%s\"";
    private static final String SPLUNK_LOGIN_LOGOUT_EVENT = "Splunk_LoginLogout_event, ";
    private static final String[] claims = new String[]{Constants.FIRST_NAME_CLAIM,
            Constants.LAST_NAME_CLAIM, Constants.COUNTRY_CLAIM};
    private final SimpleDateFormat timestampFormatter;
    private final String emptyPlaceholderValue;
    private static final String LOGIN_LOGOUT_TIME_STAMP_FORMAT = "Access.TimeStamp.Format";
    private final String accessType;

    public LoginLogoutAuditDataPublisher() {

        Properties properties = IdentityUtil.readEventListenerProperty(Constants.ABSTRACT_IDENTITY_MESSAGE_HANDLER,
                this.getClass().getName()).getProperties();
        timestampFormatter = new SimpleDateFormat(properties.getProperty(LOGIN_LOGOUT_TIME_STAMP_FORMAT, ADMIN));
        emptyPlaceholderValue = properties.getProperty(
                Constants.EMPTY_PLACEHOLDER_VALUE, Constants.UNDEFINED);
        accessType = properties.getProperty(ACCESS_TYPE, GUI);
        auditMessage = auditMessage.replaceFirst(Constants.DEFAULT_EVENT_NAME_SEPARATOR,
                properties.getProperty(Constants.EVENT_NAME_AND_SEPARATOR,
                        SPLUNK_LOGIN_LOGOUT_EVENT));
        auditMessage = auditMessage.replaceAll(Constants.DEFAULT_FIELD_SEPARATOR,
                properties.getProperty(Constants.FIELD_SEPARATOR,
                        Constants.FIELD_SEPARATOR_DEFAULT_VALUE));
    }

    @Override
    public String getName() {

        return Constants.LOGIN_LOGOUT_AUDIT_DATA_PUBLISHER;
    }

    @Override
    public void doPublishAuthenticationStepSuccess(AuthenticationData authenticationData) {
        // Nothing to implement
    }

    @Override
    public void doPublishAuthenticationStepFailure(AuthenticationData authenticationData) {
        // Nothing to implement
    }

    @Override
    public void doPublishAuthenticationSuccess(AuthenticationData authenticationData) {
        // Nothing to implement
    }

    @Override
    public void doPublishAuthenticationFailure(AuthenticationData authenticationData) {
        // Nothing to implement
    }

    @Override
    public void doPublishSessionCreation(SessionData sessionData) {

        logLoginEvent(sessionData, Constants.LOGIN);
    }

    @Override
    public void doPublishSessionTermination(SessionData sessionData) {

        logLoginEvent(sessionData, Constants.LOGOUT);
    }

    private void logLoginEvent(SessionData sessionData, String eventName) {

        if (isEnabled(null)) {
            Map<String, String> claimsAndRoles = Utils.getUserClaimsAndRoles(claims,
                    sessionData.getUserStoreDomain() + "/" + sessionData.getUser(),
                    sessionData.getTenantDomain());
            String hostname = Utils.getHostName(sessionData.getRemoteIP());

            String role = claimsAndRoles.get(Constants.ROLE_CLAIM) != null ?
                    claimsAndRoles.get(Constants.ROLE_CLAIM).replace("Application/", "")
                            .replace("Internal/everyone", "") : emptyPlaceholderValue;

            AUDIT_LOG.info(String.format(
                    auditMessage,
                    sessionData.getUser(),
                    Utils.concatenateFirstNameAndLastName(claimsAndRoles.get(Constants.FIRST_NAME_CLAIM),
                            claimsAndRoles.get(Constants.LAST_NAME_CLAIM)),
                    accessType,
                    claimsAndRoles.get(Constants.COUNTRY_CLAIM) != null ?
                            claimsAndRoles.get(Constants.COUNTRY_CLAIM) : emptyPlaceholderValue,
                    role,
                    hostname != null ? hostname : emptyPlaceholderValue,
                    sessionData.getRemoteIP(),
                    eventName,
                    timestampFormatter.format(new Date())));
        }
    }

    @Override
    public void doPublishSessionUpdate(SessionData sessionData) {
        // Nothing to implement
    }

    @Override
    public boolean isEnabled(MessageContext messageContext) {

        IdentityEventListenerConfig identityEventListenerConfig = IdentityUtil.readEventListenerProperty
                (AbstractIdentityMessageHandler.class.getName(), this.getClass().getName());

        if (identityEventListenerConfig == null) {
            return true;
        }

        return Boolean.parseBoolean(identityEventListenerConfig.getEnable());
    }

    @Override
    public boolean canHandle(MessageContext messageContext) {

        return true;
    }
}
