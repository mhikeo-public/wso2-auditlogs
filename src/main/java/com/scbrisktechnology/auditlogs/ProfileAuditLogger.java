/*
 * Developed by WSO2 Inc, for SCB Risk Technology, on request.
 */

package com.scbrisktechnology.auditlogs;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.wso2.carbon.CarbonConstants;
import org.wso2.carbon.context.CarbonContext;
import org.wso2.carbon.identity.core.AbstractIdentityUserOperationEventListener;
import org.wso2.carbon.identity.core.util.IdentityUtil;
import org.wso2.carbon.user.api.Permission;
import org.wso2.carbon.user.core.UserCoreConstants;
import org.wso2.carbon.user.core.UserStoreException;
import org.wso2.carbon.user.core.UserStoreManager;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

/**
 * User Operations listener which takes events related to role management and logs into "Audit" log.
 */
public class ProfileAuditLogger extends AbstractIdentityUserOperationEventListener {

    private static final Log audit = CarbonConstants.AUDIT_LOG;
    public static final Log LOG = LogFactory.getLog(ProfileAuditLogger.class);
    public static final String SPLUNK_PROFILE_EVENT = "Splunk_Profile_event, ";
    private static String auditMessage = "#Group_Implemented_TimeStamp=\"%s\"##" +
            "Group_Type=\"%s\"##Group_ID=\"%s\"##Group_Name=\"%s\"##" +
            "Group_Implemented_By=\"%s\"##Group_Country=\"%s\"##Group_Affected_Users=\"%s\"##Group_Status=\"%s\" ";
    private static final String USER_OPERATION_EVENT_LISTENER = "org.wso2.carbon.user.core.listener" +
            ".UserOperationEventListener";
    private static final String GROUP_TIME_STAMP_FORMAT = "Group.TimeStamp.Format";
    private static final String GROUP_TYPE = "Group.Type";
    private static final String USER = "User";
    private static final String GROUP_EMPTY_PLACEHOLDER_VALUE = "Group.EmptyPlaceholderValue";
    private final DateTimeFormatter timestampFormatter;
    private final String groupType;
    private final String groupEmptyPlaceholderValue;

    public ProfileAuditLogger() {

        Properties properties = IdentityUtil.readEventListenerProperty(USER_OPERATION_EVENT_LISTENER,
                this.getClass().getName()).getProperties();
        groupType = properties.getProperty(GROUP_TYPE, USER);
        timestampFormatter = DateTimeFormatter.ofPattern(
                properties.getProperty(GROUP_TIME_STAMP_FORMAT, Constants.ADMIN));
        groupEmptyPlaceholderValue = properties.getProperty(GROUP_EMPTY_PLACEHOLDER_VALUE,
                Constants.UNDEFINED);
        auditMessage = auditMessage.replaceFirst(Constants.DEFAULT_EVENT_NAME_SEPARATOR,
                properties.getProperty(Constants.EVENT_NAME_AND_SEPARATOR,
                        SPLUNK_PROFILE_EVENT));
        auditMessage = auditMessage.replaceAll(Constants.DEFAULT_FIELD_SEPARATOR,
                properties.getProperty(Constants.FIELD_SEPARATOR, Constants.FIELD_SEPARATOR_DEFAULT_VALUE));
    }

    @Override
    public int getExecutionOrderId() {

        return getOrderId();
    }

    @Override
    public boolean doPostDeleteRole(String roleName, UserStoreManager userStoreManager) throws UserStoreException {

        if (!isEnable()) {
            return true;
        }

        logProfileAudit(userStoreManager, roleName, Constants.DELETE, null);
        return true;
    }

    @Override
    public boolean doPostAddRole(String roleName, String[] userList, Permission[] permissions, UserStoreManager
            userStoreManager) throws UserStoreException {

        if (!isEnable()) {
            return true;
        }

        logProfileAudit(userStoreManager, roleName, Constants.CREATE, userList);
        return true;
    }

    @Override
    public boolean doPostUpdateRoleName(String roleName, String newRoleName, UserStoreManager userStoreManager)
            throws UserStoreException {

        if (!isEnable()) {
            return true;
        }

        logProfileAudit(userStoreManager, roleName, Constants.MODIFY, null);
        return true;
    }

    @Override
    public boolean doPostUpdateUserListOfRole(String roleName, String[] deletedUsers, String[] newUsers,
                                              UserStoreManager userStoreManager) throws UserStoreException {

        if (!isEnable()) {
            return true;
        }

        logProfileAudit(userStoreManager, roleName, Constants.MODIFY, (String[]) ArrayUtils.addAll(newUsers,
                deletedUsers));
        return true;
    }

    // @Override
    public boolean doPostUpdatePermissionsOfRole(String roleName, Permission[] permissions, UserStoreManager
            userStoreManager) throws UserStoreException {

        if (!isEnable()) {
            return true;
        }

        logProfileAudit(userStoreManager, roleName, Constants.MODIFY, null);
        return true;
    }

    // @Override
    public boolean doPostAddInternalRole(String roleName, String[] userList, Permission[] permissions, UserStoreManager userStoreManager) throws UserStoreException {

        if (!isEnable()) {
            return true;
        }

        logProfileAudit(userStoreManager, roleName, Constants.CREATE, userList);
        return true;
    }

    // @Override
    public boolean doPostDeleteInternalRole(String roleName, UserStoreManager userStoreManager) throws UserStoreException {

        if (!isEnable()) {
            return true;
        }

        logProfileAudit(userStoreManager, roleName, Constants.DELETE, null);
        return true;
    }

    // @Override
    public boolean doPostUpdateInternalRoleName(String roleName, String newRoleName, UserStoreManager userStoreManager) throws UserStoreException {

        if (!isEnable()) {
            return true;
        }

        logProfileAudit(userStoreManager, roleName, Constants.MODIFY, null);
        return true;
    }

    private void logProfileAudit(UserStoreManager userStore, String role, String action,
                                 String[] affectedUsers) throws UserStoreException {

        Map<String, String> claimsAndRoles = getUserClaims(userStore);
        String shortRoleName = role.replace("Application/", "").replace("Internal/everyone", "");

        audit.info(String.format(
                auditMessage,
                timestampFormatter.format(LocalDateTime.now()),
                groupType,
                shortRoleName,
                shortRoleName,
                getUser(),
                claimsAndRoles.get(Constants.COUNTRY_CLAIM) != null ?
                        claimsAndRoles.get(Constants.COUNTRY_CLAIM) : groupEmptyPlaceholderValue,
                affectedUsers != null ? Arrays.toString(affectedUsers) : groupEmptyPlaceholderValue,
                action));
    }

    /**
     * Get the logged in user's username who is calling the operation.
     *
     * @return username
     */
    private String getUser() {

        String user = CarbonContext.getThreadLocalCarbonContext().getUsername();
        if (user == null) {
            user = CarbonConstants.REGISTRY_SYSTEM_USERNAME;
        }
        return user;
    }

    private Map<String, String> getUserClaims(UserStoreManager userStore) throws UserStoreException {

        Map<String, String> claimValues = new HashMap<>();
        String username = getUser();

        if (LOG.isDebugEnabled()) {
            LOG.debug("Retrieving claims for user: " + username);
        }

        if (username == null) {
            return claimValues;
        }

        if (userStore.isExistingUser(username)) {
            claimValues.putAll(userStore.getUserClaimValues(username, new String[]{Constants.COUNTRY_CLAIM},
                    UserCoreConstants.DEFAULT_PROFILE));
        }

        return claimValues;
    }
}
