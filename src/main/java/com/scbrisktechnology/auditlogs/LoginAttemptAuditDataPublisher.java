/*
 * Developed by WSO2 Inc, for SCB Risk Technology, on request.
 */

package com.scbrisktechnology.auditlogs;

import org.apache.commons.logging.Log;
import org.wso2.carbon.CarbonConstants;
import org.wso2.carbon.identity.core.bean.context.MessageContext;
import org.wso2.carbon.identity.core.handler.AbstractIdentityMessageHandler;
import org.wso2.carbon.identity.core.model.IdentityEventListenerConfig;
import org.wso2.carbon.identity.core.util.IdentityUtil;
import org.wso2.carbon.identity.data.publisher.application.authentication.AbstractAuthenticationDataPublisher;
import org.wso2.carbon.identity.data.publisher.application.authentication.model.AuthenticationData;
import org.wso2.carbon.identity.data.publisher.application.authentication.model.SessionData;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import static org.osgi.service.useradmin.UserAdminPermission.ADMIN;

/**
 * Authentication Data Publisher which takes the login attempts and logs into "Audit" log.
 */
public class LoginAttemptAuditDataPublisher extends AbstractAuthenticationDataPublisher {

    private static final Log AUDIT_LOG = CarbonConstants.AUDIT_LOG;
    private static String auditMessage = "#Audit_ID=\"%s\"##" +
            "Audit_Name=\"%s\"##Audit_Status=\"%s\"##Audit_Country=\"%s\"##Audit_SourceHost=\"%s\"##" +
            "Audit_SourceIP=\"%s\"##Audit_LoginTimeStamp=\"%s\"";
    private static final String AUDIT_TIME_STAMP_FORMAT = "Audit.TimeStamp.Format";
    private static final String SPLUNK_AUDIT_EVENT = "Splunk_Audit_event, ";
    private static final String[] claims = new String[]{Constants.COUNTRY_CLAIM, Constants.FIRST_NAME_CLAIM,
            Constants.LAST_NAME_CLAIM};
    private final SimpleDateFormat timestampFormatter;
    private final String emptyPlaceholderValue;

    public LoginAttemptAuditDataPublisher() {

        Properties properties = IdentityUtil.readEventListenerProperty(Constants.ABSTRACT_IDENTITY_MESSAGE_HANDLER,
                this.getClass().getName()).getProperties();
        timestampFormatter = new SimpleDateFormat(properties.getProperty(AUDIT_TIME_STAMP_FORMAT, ADMIN));
        emptyPlaceholderValue = properties.getProperty(Constants.EMPTY_PLACEHOLDER_VALUE,
                Constants.UNDEFINED);
        auditMessage = auditMessage.replaceFirst(Constants.DEFAULT_EVENT_NAME_SEPARATOR, properties.getProperty(
                Constants.EVENT_NAME_AND_SEPARATOR, SPLUNK_AUDIT_EVENT));
        auditMessage = auditMessage.replaceAll(Constants.DEFAULT_FIELD_SEPARATOR,
                properties.getProperty(Constants.FIELD_SEPARATOR,
                        Constants.FIELD_SEPARATOR_DEFAULT_VALUE));
    }

    @Override
    public String getName() {

        return Constants.LOGIN_ATTEMPT_AUDIT_DATA_PUBLISHER;
    }

    @Override
    public void doPublishAuthenticationStepSuccess(AuthenticationData authenticationData) {
        // Nothing to implement
    }

    @Override
    public void doPublishAuthenticationStepFailure(AuthenticationData authenticationData) {

        logLoginAttempt(authenticationData, Constants.FAILURE);
    }

    @Override
    public void doPublishAuthenticationSuccess(AuthenticationData authenticationData) {

        logLoginAttempt(authenticationData, Constants.SUCCESS);
    }

    private void logLoginAttempt(AuthenticationData authenticationData, String status) {

        if (isEnabled(null)) {
            Map<String, String> claimsAndRoles = Utils.getUserClaimsAndRoles(claims,
                    authenticationData.getUserStoreDomain() + "/" + authenticationData.getUsername(),
                    authenticationData.getTenantDomain());
            String hostname = Utils.getHostName(authenticationData.getRemoteIp());

            AUDIT_LOG.info(String.format(
                    auditMessage,
                    authenticationData.getUsername(),
                    Utils.concatenateFirstNameAndLastName(claimsAndRoles.get(Constants.FIRST_NAME_CLAIM),
                            claimsAndRoles.get(Constants.LAST_NAME_CLAIM)),
                    status,
                    claimsAndRoles.get(Constants.COUNTRY_CLAIM) != null ?
                            claimsAndRoles.get(Constants.COUNTRY_CLAIM) : emptyPlaceholderValue,
                    hostname != null ? hostname : emptyPlaceholderValue,
                    authenticationData.getRemoteIp(),
                    timestampFormatter.format(new Date())));
        }
    }

    @Override
    public void doPublishAuthenticationFailure(AuthenticationData authenticationData) {

        logLoginAttempt(authenticationData, Constants.FAILURE);
    }

    @Override
    public void doPublishSessionCreation(SessionData sessionData) {
        // Nothing to implement
    }

    @Override
    public void doPublishSessionTermination(SessionData sessionData) {
        // Nothing to implement
    }

    @Override
    public void doPublishSessionUpdate(SessionData sessionData) {
        // Nothing to implement
    }

    @Override
    public boolean isEnabled(MessageContext messageContext) {

        IdentityEventListenerConfig identityEventListenerConfig = IdentityUtil.readEventListenerProperty
                (AbstractIdentityMessageHandler.class.getName(), this.getClass().getName());

        if (identityEventListenerConfig == null) {
            return true;
        }

        return Boolean.parseBoolean(identityEventListenerConfig.getEnable());
    }

    @Override
    public boolean canHandle(MessageContext messageContext) {

        return true;
    }

}
