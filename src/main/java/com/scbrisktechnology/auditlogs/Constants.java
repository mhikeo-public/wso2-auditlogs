/*
 * Developed by WSO2 Inc, for SCB Risk Technology, on request.
 */

package com.scbrisktechnology.auditlogs;

public class Constants {

    public static final String FAILURE = "Failure";
    public static final String SUCCESS = "Success";
    public static final String LOGIN = "Login";
    public static final String LOGOUT = "Logout";
    public static final String UNDEFINED = "Undefined";
    public static final String LOGIN_ATTEMPT_AUDIT_DATA_PUBLISHER = "LoginAttemptAuditDataPublisher";
    public static final String LOGIN_LOGOUT_AUDIT_DATA_PUBLISHER = "LoginLogoutAuditDataPublisher";
    public static final String EVENT_NAME_AND_SEPARATOR = "EventNameAndSeparator";
    public static final String FIELD_SEPARATOR = "FieldSeparator";
    public static final String EMPTY_PLACEHOLDER_VALUE = "EmptyPlaceholderValue";
    public static final String ROLE_CLAIM = "http://wso2.org/claims/role";
    public static final String FIRST_NAME_CLAIM = "http://wso2.org/claims/givenname";
    public static final String LAST_NAME_CLAIM = "http://wso2.org/claims/lastname";
    public static final String COUNTRY_CLAIM = "http://wso2.org/claims/country";
    public static final String ABSTRACT_IDENTITY_MESSAGE_HANDLER = "org.wso2.carbon.identity.core.handler." +
            "AbstractIdentityMessageHandler";
    public static final String FIELD_SEPARATOR_DEFAULT_VALUE = " ";
    public static final String DEFAULT_EVENT_NAME_SEPARATOR = "#";
    public static final String DEFAULT_FIELD_SEPARATOR = "##";
    public static final String ADMIN = "admin";
    public static final String CREATE = "Create";
    public static final String MODIFY = "Modify";
    public static final String DELETE = "Delete";
    public static final String DEFAULT_TIME_STAMP_FORMAT = "dd-MM-yyyy HH:mm:ss";

}
