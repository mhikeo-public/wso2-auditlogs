/*
 * Developed by WSO2 Inc, for SCB Risk Technology, on request.
 */

package com.scbrisktechnology.auditlogs;

import com.scbrisktechnology.auditlogs.internal.AuditLogsDSComponent;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.wso2.carbon.CarbonException;
import org.wso2.carbon.core.util.AnonymousSessionUtil;
import org.wso2.carbon.identity.data.publisher.application.authentication.AuthnDataPublisherUtils;
import org.wso2.carbon.registry.core.service.RegistryService;
import org.wso2.carbon.user.api.UserStoreException;
import org.wso2.carbon.user.core.UserCoreConstants;
import org.wso2.carbon.user.core.UserRealm;
import org.wso2.carbon.user.core.UserStoreManager;
import org.wso2.carbon.user.core.service.RealmService;
import org.xbill.DNS.Address;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

public class Utils {

    public static final Log LOG = LogFactory.getLog(AuthnDataPublisherUtils.class);

    public static Map<String, String> getUserClaimsAndRoles(String claims[], String username, String tenantDomain) {

        Map<String, String> claimValues = new HashMap<>();

        if (LOG.isDebugEnabled()) {
            LOG.debug("Retrieving claims for user: " + username + ", tenant domain: " + tenantDomain);
        }
        if (tenantDomain == null || username == null) {
            return claimValues;
        }

        try {
            UserStoreManager userStore = getUserStore(username, tenantDomain);
            if (userStore.isExistingUser(username)) {
                claimValues.put(Constants.ROLE_CLAIM, getRole(username, userStore));
                claimValues.putAll(userStore.getUserClaimValues(username,
                        claims,
                        UserCoreConstants.DEFAULT_PROFILE));
            }
        } catch (UserStoreException e) {
            LOG.error("Error when getting user store for: " + username + "@" + tenantDomain, e);
        }

        return claimValues;
    }

    public static UserStoreManager getUserStore(String username, String tenantDomain) throws UserStoreException {

        RegistryService registryService = AuditLogsDSComponent.getRegistryService();
        RealmService realmService = AuditLogsDSComponent.getRealmService();
        try {
            UserRealm realm = AnonymousSessionUtil.getRealmByTenantDomain(registryService,
                    realmService, tenantDomain);
            if (realm != null) {
                return realm.getUserStoreManager();
            } else {
                if (LOG.isDebugEnabled()) {
                    LOG.debug("No realm found. for tenant domain: " + tenantDomain + ". Hence no roles added");
                }
            }
        } catch (CarbonException e) {
            LOG.error("Error when getting realm for: " + username + "@" + tenantDomain, e);
        }
        return null;
    }

    public static String getRole(String username, UserStoreManager userStore) throws UserStoreException {

        String[] newRoles = userStore.getRoleListOfUser(username);
        return StringUtils.join(newRoles, ",");
    }

    public static String getHostName(String ipAddress) {

        try {
            return Address.getHostName(InetAddress.getByName(ipAddress)).split("\\.")[0];
        } catch (UnknownHostException e) {
            LOG.error("Error occurred while retrieving the hostname for IP: " + ipAddress);
        }
        return null;
    }

    public static String concatenateFirstNameAndLastName(String firstName, String lastName) {

        String concatenatedName = Constants.UNDEFINED;

        if (lastName != null) {
            concatenatedName = lastName;
        }

        if (firstName != null && lastName != null) {
            concatenatedName += ", " + firstName;
        } else if (firstName != null) {
            concatenatedName = firstName;
        }

        return concatenatedName;
    }
}
