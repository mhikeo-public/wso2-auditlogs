/*
 * Developed by WSO2 Inc, for SCB Risk Technology, on request.
 */

package com.scbrisktechnology.auditlogs.internal;

import com.scbrisktechnology.auditlogs.ApplicationAuditLogger;
import com.scbrisktechnology.auditlogs.LoginAttemptAuditDataPublisher;
import com.scbrisktechnology.auditlogs.LoginLogoutAuditDataPublisher;
import com.scbrisktechnology.auditlogs.ProfileAuditLogger;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;
import org.wso2.carbon.identity.application.authentication.framework.AuthenticationDataPublisher;
import org.wso2.carbon.identity.application.mgt.listener.ApplicationMgtListener;
import org.wso2.carbon.identity.core.util.IdentityCoreInitializedEvent;
import org.wso2.carbon.registry.core.service.RegistryService;
import org.wso2.carbon.user.core.listener.UserOperationEventListener;
import org.wso2.carbon.user.core.service.RealmService;

@Component(
        name = "com.scbrisktechnology.auditlogs.internal.AuditLogsDSComponent",
        immediate = true)
public class AuditLogsDSComponent {

    private static final Log log = LogFactory.getLog(AuditLogsDSComponent.class);

    private static RegistryService registryService = null;
    private static RealmService realmService = null;

    @Activate
    protected void activate(ComponentContext ctxt) {

        if (log.isDebugEnabled()) {
            log.debug("SCB Risk Technology Audit Logs bundle is activated ");
        }

        try {
            BundleContext bundleContext = ctxt.getBundleContext();
            ServiceRegistration serviceRegistration = bundleContext.registerService(
                    UserOperationEventListener.class.getName(),
                    new ProfileAuditLogger(), null);
            if (serviceRegistration == null) {
                log.error("Error while registering ProfileAuditListener.");
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("ProfileAuditLogger successfully registered.");
                }
            }

            serviceRegistration = bundleContext.registerService(ApplicationMgtListener.class.getName(),
                    new ApplicationAuditLogger(), null);
            if (serviceRegistration == null) {
                log.error("Error while registering ApplicationAuditListener.");
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("ApplicationAuditLogger successfully registered.");
                }
            }

            serviceRegistration = bundleContext.registerService(AuthenticationDataPublisher.class.getName(),
                    new LoginAttemptAuditDataPublisher(), null);
            if (serviceRegistration == null) {
                log.error("Error while registering LoginAttemptAuditDataPublisher.");
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("LoginAttemptAuditDataPublisher successfully registered.");
                }
            }

            serviceRegistration = bundleContext.registerService(AuthenticationDataPublisher.class.getName(),
                    new LoginLogoutAuditDataPublisher(), null);
            if (serviceRegistration == null) {
                log.error("Error while registering LoginLogoutAuditDataPublisher.");
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("LoginLogoutAuditDataPublisher successfully registered.");
                }
            }

        } catch (Throwable e) {
            log.error(e.getMessage(), e);
        }
    }

    @Deactivate
    protected void deactivate(ComponentContext ctxt) {

        if (log.isDebugEnabled()) {
            log.debug("SCB Risk Technology Audit Logs bundle is deactivated ");
        }
    }

    @Reference(
            name = "registry.service",
            service = org.wso2.carbon.registry.core.service.RegistryService.class,
            cardinality = ReferenceCardinality.MANDATORY,
            policy = ReferencePolicy.DYNAMIC,
            unbind = "unsetRegistryService")
    protected void setRegistryService(RegistryService registryService) {

        if (log.isDebugEnabled()) {
            log.debug("Setting the Registry Service");
        }
        AuditLogsDSComponent.registryService = registryService;
    }

    protected void unsetRegistryService(RegistryService registryService) {

        if (log.isDebugEnabled()) {
            log.debug("Unsetting the Registry Service");
        }
        AuditLogsDSComponent.registryService = null;
    }

    @Reference(
            name = "user.realmservice.default",
            service = org.wso2.carbon.user.core.service.RealmService.class,
            cardinality = ReferenceCardinality.MANDATORY,
            policy = ReferencePolicy.DYNAMIC,
            unbind = "unsetRealmService")
    protected void setRealmService(RealmService realmService) {

        if (log.isDebugEnabled()) {
            log.debug("Setting the Realm Service");
        }
        AuditLogsDSComponent.realmService = realmService;
    }

    protected void unsetRealmService(RealmService realmService) {

        if (log.isDebugEnabled()) {
            log.debug("Unsetting the Realm Service");
        }
        AuditLogsDSComponent.realmService = null;
    }

    @Reference(
            name = "IdentityCoreInitializedEvent",
            service = org.wso2.carbon.identity.core.util.IdentityCoreInitializedEvent.class,
            cardinality = ReferenceCardinality.MANDATORY,
            policy = ReferencePolicy.DYNAMIC,
            unbind = "unsetIdentityCoreInitializedEventService")
    protected void setIdentityCoreInitializedEventService(IdentityCoreInitializedEvent identityCoreInitializedEvent) {
    /* reference IdentityCoreInitializedEvent service to guarantee that this component will wait until identity core
         is started */
    }

    protected void unsetIdentityCoreInitializedEventService(IdentityCoreInitializedEvent identityCoreInitializedEvent) {
    /* reference IdentityCoreInitializedEvent service to guarantee that this component will wait until identity core
         is started */
    }

    public static RegistryService getRegistryService() {

        return registryService;
    }

    public static RealmService getRealmService() {

        return realmService;
    }

}

