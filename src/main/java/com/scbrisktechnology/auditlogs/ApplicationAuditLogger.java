/*
 * Developed by WSO2 Inc, for SCB Risk Technology, on request.
 */

package com.scbrisktechnology.auditlogs;

import org.apache.commons.logging.Log;
import org.wso2.carbon.CarbonConstants;
import org.wso2.carbon.identity.application.common.IdentityApplicationManagementException;
import org.wso2.carbon.identity.application.common.model.ServiceProvider;
import org.wso2.carbon.identity.application.mgt.listener.AbstractApplicationMgtListener;
import org.wso2.carbon.identity.application.mgt.listener.ApplicationMgtListener;
import org.wso2.carbon.identity.core.model.IdentityEventListenerConfig;
import org.wso2.carbon.identity.core.util.IdentityUtil;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.Properties;

/**
 * Application Management listener which takes the events related to application management and logs into "Audit" log.
 */
public class ApplicationAuditLogger extends AbstractApplicationMgtListener {

    private static final Log audit = CarbonConstants.AUDIT_LOG;
    public static final String SPLUNK_ID_EVENT = "Splunk_ID_event, ";
    private static String auditMessage = "#AppID_ID=\"%s\"##" +
            "AppID_Profile=\"%s\"##AppID_Name=\"%s\"##AppID_Type=\"%s\"##" +
            "AppID_Implemented_By=\"%s\"##AppID_Implemented_TimeStamp=\"%s\"##AppID_Country=\"%s\"##" +
            "AppID_Group=\"%s\"##AppID_Status=\"%s\" ";
    private static final String APPLICATION_MGT_LISTENER_TYPE = "org.wso2.carbon.identity.application.mgt.listener." +
            "AbstractApplicationMgtListener";
    private static final String APP_ID_TYPE = "AppID.Type";
    private static final String APP_ID_GROUP = "AppID.Group";
    private static final String APP_ID_TIME_STAMP_FORMAT = "AppID.TimeStamp.Format";
    private static final String EMPTY_PLACEHOLDER_VALUE = "EmptyPlaceholderValue";
    private static final String VIEW_USER = "VIEW_User";
    private static final String[] claims = new String[]{Constants.COUNTRY_CLAIM};

    private final String appIdType;
    private final String appIdGroup;
    private final String appIdEmptyPlaceholderValue;
    private final DateTimeFormatter timestampFormatter;

    public ApplicationAuditLogger() {

        Properties properties = IdentityUtil.readEventListenerProperty(APPLICATION_MGT_LISTENER_TYPE,
                this.getClass().getName()).getProperties();
        appIdType = properties.getProperty(APP_ID_TYPE, VIEW_USER);
        appIdGroup = properties.getProperty(APP_ID_GROUP, Constants.DEFAULT_TIME_STAMP_FORMAT);
        timestampFormatter = DateTimeFormatter.ofPattern(properties.getProperty(APP_ID_TIME_STAMP_FORMAT,
                Constants.ADMIN));
        appIdEmptyPlaceholderValue = properties.getProperty(EMPTY_PLACEHOLDER_VALUE,
                Constants.UNDEFINED);
        auditMessage = auditMessage.replaceFirst(Constants.DEFAULT_EVENT_NAME_SEPARATOR, properties
                .getProperty(Constants.EVENT_NAME_AND_SEPARATOR,
                        SPLUNK_ID_EVENT));
        auditMessage = auditMessage.replaceAll(Constants.DEFAULT_FIELD_SEPARATOR, properties.getProperty(
                Constants.FIELD_SEPARATOR, Constants.FIELD_SEPARATOR_DEFAULT_VALUE));
    }

    @Override
    public int getDefaultOrderId() {

        return 205;
    }

    @Override
    public boolean doPostCreateApplication(ServiceProvider serviceProvider, String tenantDomain, String username)
            throws IdentityApplicationManagementException {

        if (!isEnable()) {
            return true;
        }
        logApplicationAudit(serviceProvider, tenantDomain, username, Constants.CREATE);
        return true;
    }

    @Override
    public boolean doPostUpdateApplication(ServiceProvider serviceProvider, String tenantDomain, String username)
            throws IdentityApplicationManagementException {

        if (!isEnable()) {
            return true;
        }
        logApplicationAudit(serviceProvider, tenantDomain, username, Constants.MODIFY);
        return true;
    }

    @Override
    public boolean isEnable() {

        IdentityEventListenerConfig identityEventListenerConfig = IdentityUtil.readEventListenerProperty
                (AbstractApplicationMgtListener.class.getName(), this.getClass().getName());

        if (identityEventListenerConfig == null) {
            return true;
        }

        return Boolean.parseBoolean(identityEventListenerConfig.getEnable());
    }

    public boolean doPostDeleteApplication(ServiceProvider serviceProvider, String tenantDomain, String username)
            throws IdentityApplicationManagementException {

        if (!isEnable()) {
            return true;
        }
        logApplicationAudit(serviceProvider, tenantDomain, username, Constants.DELETE);
        return true;
    }

    private void logApplicationAudit(ServiceProvider serviceProvider, String tenantDomain, String username,
                                     String action) {

        int appId = -1;
        String name = Constants.UNDEFINED;
        if (serviceProvider != null) {
            appId = serviceProvider.getApplicationID();
            name = serviceProvider.getApplicationName();
        }

        Map<String, String> claimsAndRoles = Utils.getUserClaimsAndRoles(claims, username, tenantDomain);
        String role = claimsAndRoles.get(Constants.ROLE_CLAIM) != null ?
                claimsAndRoles.get(Constants.ROLE_CLAIM).replace("Application/", "")
                        .replace("Internal/everyone", "") : appIdEmptyPlaceholderValue;

        audit.info(String.format(
                auditMessage,
                appId,
                role,
                name,
                appIdType,
                username,
                timestampFormatter.format(LocalDateTime.now()),
                claimsAndRoles.get(Constants.COUNTRY_CLAIM) != null ? claimsAndRoles.get(Constants.COUNTRY_CLAIM)
                        : appIdEmptyPlaceholderValue,
                appIdGroup,
                action));
    }

}
